function loadFilms() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => {
      if (!response.ok) {
        throw new Error("url addres is not working");
      }
      return response.json();
    })
    .then((data) => {
      console.log(data);
      render(data);
    })
    .catch((error) => console.log(error));
}
loadFilms();

function loadChars(url, ul) {
  // console.log(ul);
  fetch(url)
    .then((response) => {
      if (!response.ok) {
        throw new Error("url addres is not working");
      }
      return response.json();
    })
    .then((data) => {
      setTimeout(() => {
        document.querySelector(ul).classList.add("loaded");
        renderChar(data, ul);
      }, 1000);
    })
    .catch((error) => console.log(error));
}

function render(arr) {
  let filmList = document.getElementById("filmList");
  arr.forEach((obj) => {
    let film = `<div class="film">
    <h3>${obj.name}</h3>
    <ul id="film-${obj.episodeId}"><span>loading...</span></ul>
    <p>${obj.episodeId}</p>
    <p>${obj.openingCrawl}</p>
</div>`;

    filmList.innerHTML += film;
    obj.characters.forEach((url) => {
      const ul = `#film-${obj.episodeId}`;
      // console.log(ul);
      loadChars(url, ul);
      //  console.log(char);
    });
  });
}

function renderChar(obj, ul) {
  const li = `<li>${obj.name}</li>`;
  document.querySelector(ul).innerHTML += li;
  // console.log(li);
}
